
function calcAge(birthday) {
    const [day, month, year] = birthday.split(".");
    const diff = new Date - new Date(month + "." + day + "." + year);
    return new Date(diff).getFullYear() - 1970;
}

function createNewUser(firstName, lastName){
    const birthday = prompt ('введите дату (в формате dd.mm.yyyy)','');
    const newUser = {
        firstName, lastName, birthday,
        getAge() {
            return calcAge(birthday)
        },
        getLogin() {
            return (firstName[0] + lastName).toLowerCase()
        },
        getPassword() {
            return firstName.toUpperCase()[0] + lastName.toLowerCase() + birthday.slice(-4);
        }
    }
    return newUser
}


let user = createNewUser("Ivan", "Kravchenko");
console.log(user.getLogin(), user.getAge(), user.getPassword());
