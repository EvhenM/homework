function arrToList(arr) {
    const list = document.createElement('ul');
    list.innerHTML = arr.map(item => `<li>${item}</li>`).join('');
    document.body.append(list);
}
arrToList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);