let arr = ['hello', 'world', 23, '23', null];

function filterBy(arr, type) {
    const newArr = [];
    for ( let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === type){
            continue
        }
        newArr.push(arr[i])
    }
    return newArr;
}

console.log(filterBy(arr, "number"));