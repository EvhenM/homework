window.onload = function() {
    document.querySelector('.tabs').addEventListener('click',fTabs)

    function fTabs (event) {
        let dataTab = event.target.getAttribute('data-tab');
        let tabBody = document.getElementsByClassName('tabs-text');
        for (let i = 0; i < tabBody.length; i++) {
            if ( dataTab==i) {
                tabBody[i].style.display = 'block';
            } else {
                tabBody[i].style.display = 'none';
            }
        }
    }
}